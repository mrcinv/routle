export type CityDistance = {
    city: string
    distance: number
}

export type DistanceGraph = {
    [key: string]: CityDistance[]
} 

// Data structure that is used to perform Dijkstra's algorithm for shortest path search
export type ShortestPathData = {
    distances: { [key: string]: number }
    parents: { [key: string]: string }
    priorityQueue: string[]
    source: string
}

// Create initial data for search of shortest path between two nodes in a graph
export const initialSPData = (source: string, graph: DistanceGraph): ShortestPathData => {
    const vertices = Object.keys(graph)
    const distances : {[key: string]: number} = {}
    vertices.forEach(element => { distances[element] = Infinity })
    distances[source] = 0
    const priorityQueue = vertices
    priorityQueue.sort((a, b) => distances[a] - distances[b])
    const parents: { [key: string]: string } = {}
    return {distances, source, priorityQueue, parents}
}

// Update distance on nodes that are adjecent to the current pivot and sort priority queue.
// The distances and priority queue are updated in place.
export const updateDistances = (spData: ShortestPathData, graph: DistanceGraph) => {
    const {
        distances,
        parents,
        priorityQueue
    } = spData
    const pivot = priorityQueue.shift() 
    if (pivot === undefined) return
    const baseDistance = distances[pivot]
    graph[pivot].forEach(
        (cityDistance) => {
            const city = cityDistance.city
            const newDistance = cityDistance.distance + baseDistance
            if (newDistance < distances[city]) {
                parents[city] = pivot
                distances[city] = newDistance
            }
        }
    )
    priorityQueue.sort((a, b) => distances[a] - distances[b])
}

// Check if the search has reahed the target or the queue is empty.
export const continueSearch = (spData: ShortestPathData, target: string) => {
    const {
        priorityQueue,
        distances
    } = spData
    const pivot = priorityQueue[0]
    return (pivot != undefined) && (distances[target] > distances[pivot])
}

// Extract the shortest path from the given data    
const shortestPathFromTarget = (spData: ShortestPathData, target: string) => {
    const path: string[] = [target]
    const parents = spData.parents
    let parent = parents[path[0]]
    while (parent)
    {
        path.unshift(parent)
        parent = parents[parent]
    }
    return path
}

// Find the shortest path in the graph from source to target
export const shortestPath = (
    source: string,
    target: string,
    graph: DistanceGraph,
    pathData?: ShortestPathData) => {
    const spData = (pathData == undefined) ? initialSPData(source, graph) : pathData
    while (continueSearch(spData, target)) {
        updateDistances(spData, graph)
    }
    return shortestPathFromTarget(spData, target)
}