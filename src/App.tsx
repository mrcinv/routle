import { useState } from 'react'
import './App.css'
import { Autocomplete, TextField } from '@mui/material';
import { DistanceGraph, ShortestPathData, shortestPath } from './dijkstra';

type Guess = {
  city: string
  direction: string
}

const displayGuesses = (guesses: Guess[]) => {
  return Object.values(guesses).map(
    (guess: Guess) => {
      const value = guess.city + " → " + guess.direction
      return (
        <div>
          <TextField type="text" value={value} disabled></TextField>
        </div>
      )
    }
  )
}

// Find direction to follow the shortest path from guessed City to hidden city
const findDirection = (
  guessedCity: string,
  hiddenCity: string,
  graph: DistanceGraph,
  pathData?: ShortestPathData
) => {
  if (guessedCity === hiddenCity) {
    return hiddenCity    
  }
  const path = shortestPath(hiddenCity, guessedCity, graph, pathData)
  if (path.length < 3) {
    return hiddenCity
  }
  return path[path.length - 2]
}

type AppProps = {
  targetCity: string,
  graph: DistanceGraph
}

const initialGuesses : Guess[] = []

function App(props: AppProps) {
  const {
    targetCity,
    graph } = props
  const cities = Object.keys(graph)
  const [guesses, setGuesses] = useState(initialGuesses)
  const [value, setValue] = useState('');
  const guessedCities = guesses.map((guess) => guess.city)
  const guessedCity = guessedCities[guessedCities.length - 1] || "not guessed yet"
  const options = cities.filter((city)=> !guessedCities.includes(city))
  const onChange = (
    e: React.SyntheticEvent<Element, Event>,
    value: string | undefined,
    reason: any
  ) => {
    if (value === undefined) {
      return
    }
    if (value in graph) {
      const direction = findDirection(value, targetCity, graph)
      setValue('')
      setGuesses(guesses.concat(
        [{ city: value, direction: direction }])
      )
      return
    }
    setValue(value)
  }
  const gameOver = guessedCity === targetCity
  const cityInput = <Autocomplete
    options={options}
    disabled={gameOver}
    onInputChange={onChange}
    value={value}
    renderInput={(params) => <TextField {...params} label="Guess the city" />} />;
  
  const gameOverText = gameOver ?
    <div>The city is: <strong>{targetCity}</strong>. You needed <strong>{guesses.length}</strong> steps!</div> :
    <></>

  return (
    <div className="App">
      <h1>Routle</h1>
      <h3>A geography puzle</h3>
      <div className="card">
        <p>
          Guess a city in Europe. The app will point you in the direction of that city.
        </p>
        <p>
          {displayGuesses(guesses)}
        </p>
        <p>
          {gameOver ? gameOverText : cityInput}</p>
      </div>
    </div>
  )
}

export default App
