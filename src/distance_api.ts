import axios from 'axios'
import { distances } from './distances'

const url = 'https://www.distance24.org/route.json?stops='

export type getDistanceFunc = 
    (c: string, oc: string) => Promise<number>

export const getDistance : getDistanceFunc = async (city: string, otherCity: string) => {
    return axios.get(url + city + "|" + otherCity).then(
        (response) => response.data.distances
    )
}

export const getRealDistances = async () => Promise.all(
        distances.map(
            ([city, otherCity, _distance]) =>
                getDistance(city, otherCity).then(
                    (realDistance) => {
                        console.log("[\"" + city + "\", \""
                            + otherCity + "\", " + realDistance + "],")
                        return [city, otherCity, realDistance]
                    }
                ))
)

getRealDistances().then((distances) => console.log(distances))