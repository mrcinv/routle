import { test, expect } from "vitest";
import {
    cityOfTheDay,
    daysFromMiliseconds,
    prandomInteger,
    period
} from "./city_of_the_day";
import { cityGraph } from "./city_graph";

const range = (start: number, end: number) => new Array(end - start + 1)
.fill(undefined).map((_value, index) => index + start)

test("days", () => {
    expect(daysFromMiliseconds(1000*60*60*24*5)).toEqual(5)
})

test("pseudo random generator", () => {
    // period should not divide the number of cities
    expect(Object.keys(cityGraph).length % period).not.toEqual(0)
    // generated numbers should be in range
    range(1000, 2000).forEach((index) => {
        const integer = prandomInteger(index, 0, 120)
        expect(integer).toBeGreaterThanOrEqual(0)
        expect(integer).toBeLessThanOrEqual(120)
    })
})

test("city of the day", () => {
    expect(cityOfTheDay(1671948058398, cityGraph)).toEqual("Narvik")
})