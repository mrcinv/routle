import { getDistance } from "./distance_api";
import { test, expect } from 'vitest'

test("distance api", async () => {
    expect(getDistance("Ljubljana", "Zagreb")).resolves.toEqual(117)
})