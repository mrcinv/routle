import { test, expect } from "vitest";
import { shortestPath, updateDistances, initialSPData } from "./dijkstra";
import { cityGraph } from "./city_graph";

test('shortest path', () => {
    const path = shortestPath("Ljubljana", "Ljubljana", cityGraph)
    expect(path).toEqual(["Ljubljana"])
    const path1 = shortestPath("Ljubljana", "Zagreb", cityGraph)
    expect(path1).toEqual(["Ljubljana", "Zagreb"])
    const path2 = shortestPath("Ljubljana", "Goteborg", cityGraph)
    expect(path2).toEqual([
        "Ljubljana", "Salzburg", "Linz", "Praha", "Dresden", "Berlin",
        "Hamburg", "Luebeck", "Kobenhaven", "Malmö", "Goteborg"
    ])
    const path3 = shortestPath("Tirana", "Ljubljana", cityGraph)
    expect(path3).toEqual(["Tirana", "Dubrovnik", "Split", "Zagreb", "Ljubljana"])
})

test('update distances', () => {
    const startData = initialSPData("Ljubljana", cityGraph)
    const startLength = startData.priorityQueue.length
    expect(startLength).toEqual(Object.keys(cityGraph).length)
    updateDistances(startData, cityGraph)
    expect(startData.priorityQueue.length).toEqual(startLength - 1)
    expect(startData.parents).toEqual({
        "Zagreb": "Ljubljana",
        "Salzburg": "Ljubljana",
        "Trst": "Ljubljana",
        "Graz": "Ljubljana"
    })
    expect(startData.priorityQueue.slice(0, 3)).toEqual([
        "Zagreb",
        "Trst",
        "Graz"
    ])
    expect(startData.distances["Zagreb"]).toEqual(100)
})

test("Update distances 2", () => {
    const startData = initialSPData("Ljubljana", cityGraph)
    const startLength = startData.priorityQueue.length
    updateDistances(startData, cityGraph)
    updateDistances(startData, cityGraph)
    expect(startData.priorityQueue.length).toEqual(startLength - 2)
    expect(startData.priorityQueue[0]).toEqual("Trst")
    expect(startData.priorityQueue).not.toContain("Zagreb")
    expect(startData.distances["Split"]).toEqual(400)
    expect(startData.parents).toEqual({
        "Beograd": "Zagreb",
        "Budapest": "Zagreb",
        "Graz": "Ljubljana",
        "Salzburg": "Ljubljana",
        "Sarajevo": "Zagreb",
        "Split": "Zagreb",
        "Trst": "Ljubljana",
        "Zagreb": "Ljubljana",
    })
    expect(startData.priorityQueue.slice(0, 6)).toEqual(
        ["Trst","Graz", "Salzburg", "Split", "Sarajevo","Beograd",]
    )
    expect(startData.distances["Budapest"]).toBeGreaterThan(startData.distances["Sarajevo"])
})