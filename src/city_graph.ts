import { distances, DistanceList } from "./distances"
import { DistanceGraph } from "./dijkstra"

export const calculateGraph = (distances: DistanceList) => {
    const cityGraph: DistanceGraph = {}
    distances.forEach(([city1, city2, distance]) => {
        if (cityGraph[city1] == undefined) {
            cityGraph[city1] = []
        }
        if (cityGraph[city2] == undefined) {
            cityGraph[city2] = []
        }
        cityGraph[city1].push({ city: city2, distance: distance })
        cityGraph[city2].push({ city: city1, distance: distance })
    })
    return cityGraph
}

export const cityGraph = calculateGraph(distances)