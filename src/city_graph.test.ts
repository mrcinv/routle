import { calculateGraph, cityGraph } from "./city_graph"
import { test, expect } from 'vitest'

test("city graph", () => {
    expect(cityGraph["Ljubljana"]).toEqual(
        [
            { city: "Zagreb", distance: 100 },
            { city: "Trst", distance: 100 },
            { city: "Salzburg", distance: 250},
            { city: "Graz", distance: 200 },
        ]
    )
})

test("calculate graph", () => {
    const distances : [string, string, number][] = [["a", "b", 100], ["a", "c", 200]]
    const graph = calculateGraph(distances)
    expect(graph["a"]).toEqual([
        {city: "b", distance: 100}, { city: "c", distance: 200}
    ])
})