import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { cityGraph } from './city_graph'
import { cityOfTheDay } from './city_of_the_day'

//const cityOfTheDay = "Ljubljana"
const targetCity = cityOfTheDay(Date.now(), cityGraph)

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App
      targetCity={targetCity}
      graph={cityGraph}
    />
  </React.StrictMode>,
)
