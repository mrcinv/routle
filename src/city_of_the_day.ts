import { DistanceGraph } from "./dijkstra"

export const period = 13

// Pure man's random integer generator. For each 
export const prandomInteger = (index: number, min: number, max: number) => {
  const size = max - min + 1
  return (index * period) % size + min
}

// Determine the city of the day
export const cityOfTheDay = (miliseonds: number, graph: DistanceGraph) => {
    const cities = Object.keys(graph)
    const num_of_cities = cities.length
    const days = daysFromMiliseconds(miliseonds)
    const index = prandomInteger(days, 0, num_of_cities)
    return cities[index]
  }

export function daysFromMiliseconds(miliseonds: number) {
  return Math.floor(miliseonds / 1000 / 3600 / 24)
}
